# It should be easy for compiler to optimize this loop.
# This example should work with all runtimes, because it
# does not require anything special.

for i in range(1000000):
    x = i * i
